/*
 * cw2.c
 *
 *  Created on: Jan 15, 2021
 *      Author: root
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <linux/if_ether.h>
#include <netinet/in.h>


int main(void) {
	printf("Uruchamiam odbieranie ramek Ethernet.\n"); /* prints */

	//Utworzenie bufora dla odbieranych ramek Ethernet
	unsigned char *buffer = (void*) malloc(ETH_FRAME_LEN);

	//Otwarcie gniazda pozwalającego na odbiór ramek wszystkich ramek
	int iEthSockHandl = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

	//Kontrola czy gniazdo zostało otwarte poprawnie, w przypadku bledu wyświetlenie komunikatu.
	if (iEthSockHandl<0)
		printf("Problem z otwarciem gniazda : %s!\n", strerror(errno));

	//Zmienna do przechowywania rozmiaru odebranych danych
	int iDataLen = 0;

	//Pętla nieskończona do odbierania ramek Ethernet
	while (1) {

		//Odebranie ramki z utworzonego wcześniej gniazda i zapisanie jej do bufora
		iDataLen = recvfrom(iEthSockHandl, buffer, ETH_FRAME_LEN, 0, NULL, NULL);

		 //Kontrola czy nie było bledu podczas odbierania ramki
		if (iDataLen == -1)
			 printf("Nie moge odebrac ramki: %s! \n", strerror(errno));
		else { //jeśli ramka odebrana poprawnie wyświetlenie jej zawartości
			printf("\nOdebrano ramke Ethernet o rozmiarze: %d [B]\n", iDataLen);

			if(buffer[12]==0x08 && buffer[13]==0x00){
				printf("Version : %01x\n", (0x0F & (buffer[14] >>4)));
			 	printf("Header Length : %01x\n", (buffer[14] & 0x0F));
				printf("Differentiated Services Field: %02x\n", buffer[15]);
				printf("Total Length: %02x %02x\n", buffer[16],buffer[17]);
				printf("Identification: %02x %02x\n", buffer[18],buffer[19]);
				printf("Flags: %02x %02x\n", buffer[20],buffer[21]);
				printf("Time to live: %02x \n", buffer[22]);
				printf("Protocol: %02x \n", buffer[23]);
				printf("Header checksum: %02x %02x\n", buffer[24],buffer[25]);
				printf("-Adres IP nadawcy:       %i.%i.%i.%i\n", buffer[26],buffer[27],buffer[28],buffer[29]);
				printf("-Adres IP odbiorcy:       %i.%i.%i.%i\n", buffer[30],buffer[31],buffer[32],buffer[33]);
				//printf("Source: %02x.%02x.%02x.%02x\n", buffer[25],buffer[26],buffer[27],buffer[28]);
				//printf("Source: %02x.%02x.%02x.%02x\n", buffer[29],buffer[30],buffer[31],buffer[32]);
			}

		 }
	}
		 return EXIT_SUCCESS;
	}




